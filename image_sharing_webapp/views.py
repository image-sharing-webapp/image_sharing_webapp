from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

def home(request):
    # return HttpResponse('Hello!')
    user = request.user
    hello = "Hello!"

    context = {
        'user': user,
        'hello': hello,
    }
    return render(request, 'main/home.html', context)
